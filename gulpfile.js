// Include gulp
var gulp = require('gulp');
 // Define base folders
var src = 'src/';
var dest = 'build/';
 // Include plugins
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-css');
var rename = require('gulp-rename');

 // Concatenate & Minify JS
gulp.task('js', function() {
     gulp.src('public/js/*.js')
      .pipe(concat('ui.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('resources/assets/js/'));
});
 // Compile CSS files
gulp.task('style', function(){
   gulp.src('public/css/*.css')
   .pipe(concat('style.css'))
   .pipe(minify())
   .pipe(gulp.dest('resources/assets/css/'));
});
 // Watch for changes in files
gulp.task('watch', function() {
   // Watch .js files
  gulp.watch('public/js/*.js', ['js']);
   // Watch .css files
  gulp.watch('public/css/*.css', ['style']);
 });
 // Default Task
gulp.task('default', ['js', 'style', 'watch']);